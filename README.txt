Curso de Udemy: "Experto en Bootstrap 4 en 7 días (Crea un sitio web real)" por Erick Mines
Duración: 21 horas

Herramientas:
* https://bootsnipp.com/search?q=form
* https://www.bootdey.com/bootstrap-snippets?utf8=%E2%9C%93&q=form
* https://startbootstrap.com/
* Lector de pantalla: jaws
* https://www.codeply.com/
* https://www.tutorialrepublic.com/ 
* Botón de cita: https://html-css-js.com/css/generator/transform/
* Slider: https://swiperjs.com/
* https://veno.es/venobox/ (video 84, es ligero, con multiples opciones de configuracion y gran comunidad para soluciones/soporte, reproducción de video automático)
* https://www.jqueryscript.net/lightbox/
* lightcase.js para fotos
* Efecto parallax (video 90): https://github.com/nk-o/jarallax
* Para que IE entienda la etiqueta picture (video 95) pollify: https://scottjehl.github.io/picturefill/
* (Video 100) libreria para counter up: http://imakewebthings.com/waypoints/
* Video 100 animación contador: https://github.com/ciromattia/jquery.counterup
* Script para el calendario de fecha y hora usado (video 118): https://amsul.ca/pickadate.js/
* Validador de campos de formulario simple: https://bootstrap-validate.js.org/
* Validacion (video 126): http://parsleyjs.org/
* Configuración de parsley con bootstrap (video 135): https://devhints.io/parsley
* Barra de navegación fija (video 145): https://github.com/emn178/jquery-stickit
* Scroll de navegacion (mejor que smooth usado en otro proyecto, video )148: https://github.com/malihu/page-scroll-to-id 
* Efectos de animacion para el scroll (video 149): https://api.jqueryui.com/easings/
* Correcciones css para versiones de IE (video 150): https://github.com/ridjohansen/css_browser_selector

Extensiones para visual studio code:
* Live Server
* Beautify css/sass/scss/less
* HTML Class Suggestions 
* IntelliSense for CSS class names in HTML

NOTAS:
text-black-50 para color de texto negro ligero o grisaseo (video 78)

/*Para centrar el botón del video*/ (video 82)
position: absolute;
left: 50%;
top: 50;
transform: translate(-50%,-50%);

/*Para que el borde en estado hover al hacerlo transparente no agarre el color del boton y sea por fuera*/ (video 83)
background-clip: padding-box; /*otros valores son content-box; border-box viene por defecto*/

text-truncate; (overflow-hidden)

Margenes negativos en bootstrap (video 88): mt-n1, mt-lg-n2 